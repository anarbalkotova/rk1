<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    public function index() {
        $questions = Question::paginate(5);
        return view('index', ['questions' => $questions]);
    }

    public function newQuestion() {
        return view('new');
    }

    public function create(Request $request) {
        $question = new Question();
        $question->question = $request->get('question');
        $question->save();
        return redirect('/questions');
    }
}
