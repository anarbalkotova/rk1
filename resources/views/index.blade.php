@extends('layout')

@section('content')
    <a href="/questions/new" class="btn btn-info">Создать вопрос</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Вопрос</th>
                <th>Дата создания</th>
            </tr>
        </thead>
        <tbody>
            @foreach($questions as $question)
                <tr>
                    <td>{{$question->id}}</td>
                    <td>{{$question->question}}</td>
                    <td>{{$question->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>


    {{$questions->links()}}
@endsection